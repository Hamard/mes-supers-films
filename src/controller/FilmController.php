<?php

class FilmController
{

    public $conn;

    public function __construct()
    {
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    public function getFilmById($id)
    {

        $query = "
            SELECT
                title,
                realisateur_id,
                jaquette,
                description, 
                note,
                duree
            FROM
                films
            WHERE
                id = :id
                ";

        $stmt = $this->conn->prepare($query);
        $stmt->execute([':id' => $id]);
        $film = new Film();
        if ($stmt->rowCount() > 0) {
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            
            $film->setId($id);
            $film->setTitle($res['title']);
            $film->setDesc($res['description']);
            $film->setJaquette($res['jaquette']);
            $film->setRealisateur($res['realisateur_id']);
            $film->setNote($res['note']);
            $film->setDuree($res['duree']);
        }
        return $film;
    }

    public function getFilms()
    {
        $query = "
        SELECT
            f.id,
            f.title,
            f.description as film_description,
            r.name,
            f.jaquette
        FROM
            films f
        INNER JOIN
            realisateurs r
        ON
            r.id = f.realisateur_id
        INNER JOIN
            medias m
        ON
            m.film_id = f.id
            order by f.title ASC
        ";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        // var_dump($stmt);
        // var_dump($stmt->fetch());

        $res = array();

        // var_dump($stmt->fetchAll());
        foreach ($stmt->fetchAll() as $film) {
            // var_dump($film);
            $f = new Film();
            $f->setId($film['id']);
            $f->setTitle($film['title']);
            $f->setDesc($film['film_description']);
            $f->setJaquette($film['jaquette']);
            $res[] = $f;
        }

        // var_dump($res);
        return $res;
    }
}
